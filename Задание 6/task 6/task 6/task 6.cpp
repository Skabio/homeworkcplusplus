﻿#include <iostream>

using namespace std;

int main()
{
    //Задание 6. Маятник
    setlocale(LC_ALL, "Russian");

    double Peercent = 8.4;
    int Cycle = 0;
    double Amplitude;
    double Amplitude_final;

    cout << "Начальная амплитуда \n";
    cin >> Amplitude;
    cout << "Конечная амплитуда амплитуда \n";
    cin >> Amplitude_final;

    while (Amplitude > Amplitude_final)
    {
        Amplitude = Amplitude - ((Amplitude * Peercent) / 100);
        Cycle++;
    }

    cout << "Колличество колебаний маятника ровняется " << Cycle;
}


