﻿#include <iostream>

using namespace std;

int main()
{
    //Задание 8. Ёлочка

    setlocale(LC_ALL, "Russian");

    int height;

    cout << "Введите высоту треугольника: ";
    cin >> height;

    for (int i = 1; i <= height; ++i) {
        for (int j = 0; j < height - i; ++j) {
            cout << " ";
        }
        for (int j = 0; j < 2 * i - 1; ++j) {
            cout << "#";
        }

        cout << "\n";
    }
}