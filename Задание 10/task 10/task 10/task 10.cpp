﻿#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");
    //Задача 10. Угадай число наоборот.
        int max = 63;
        int min = 0;
        int differ;
        char ansver;
        
        cout << "Давай поиграем в угадайку. Загадай число от 0 до 63\n";
        cout << "Для ответа используй символы Y(Да), N(Нет).\n";

        while (min != max)
        {   
            differ = (max + min) / 2;
            cout << "Ваше число равно " << differ << "?\n";
            cin >> ansver;
            if (ansver == 'Y')
            {
                break;
            }
            do
            {  
                cout << "Твое число меньше " << differ << "?\n";
                cin >> ansver;
            }
            while (ansver != 'Y' && ansver != 'N');

            if (ansver == 'Y')
            {
                max = differ-1;
            }
            else if (ansver == 'N')
            {
                min = differ+1;
            }   
            if (min == max)
                differ = min;
        }    
        cout << "Ваше число " << differ << "\n";
}

