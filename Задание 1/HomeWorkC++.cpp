﻿#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	//Задание 1. Космический симулятор

	float F, m, t, S;

	cout << "Введите силу тяги F, время полетов t, массу m звездолета\n";
	cin >> F >> m >> t;

	if (F <= 0 || m <= 0 || t <= 0)
	{
		cout << "Не верные данные";
		return 1;
	}

	S = ((F / m) * t * t) / 2;

	cout << "Расстояние, которе пролетел звездолет " << S;

}

