﻿#include <iostream>
using namespace std;
int main()
{
	//Задание 5. Кенийский бегун
	setlocale(LC_ALL, "Russian");

	int kilometr;
	int min;
	int sec = 0;
	int num;
	int count;

	cout << "Привет, Сэм! Сколько километров ты сегодня пробежал? ";
	cin >> num;

	for (int i = 0; i < num; i++)
	{
		cout << "Какой у тебя был темп на километре " << (i)+1 << " ? ";
		cin >> count;
		sec += count;
	}

	sec = sec / num;
	min = sec / 60;
	sec = sec - (min * 60);

	cout << "Твой средний темп за тренировку: " << min << " минут " << sec << " секунд ";
}
