﻿#include <iostream>
using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");

    //Задание 7. Биолаборатория
    
    int Bacteria, Antibiotic;
    int pow = 10;

    std::cout << "Введите количество бактерий: ";
    std::cin >> Bacteria;
    std::cout << "Введите количество антибиотика: ";
    std::cin >> Antibiotic;

    for (int hour = 1; Bacteria > 0 && pow!=0; ++hour) {

        Bacteria *= 2;

        if (pow != 0)
        {
            Bacteria -= pow*Antibiotic;
            pow--;
        }
        cout << "После " << hour << " часа бактерий осталось " << Bacteria <<"\n";

    }

}

