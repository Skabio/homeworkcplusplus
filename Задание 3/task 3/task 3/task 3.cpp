﻿#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	//Задание 3. Игрушечная история

    double x, y, z;
    int NumCubes;
    int Num_Set;
    int quantity;
    int size;

    cout << "Введите размкры бруска (х, у, z): ";
    cin >> x >> y >> z;
    cout << "Введите предпочитаемое колличество в наборе и предпочитаемый размер куба (размер, колличество в набоое) ";
    cin >> size >> quantity;

    if (x <= 1 || y <= 1 || z <= 1 || size < 5 || quantity < 8)
    {
        cout << "Некоректный ввод\n";
        return 1;
    }

    size = size * size * size;
    NumCubes = x * y * z / size;
    Num_Set = NumCubes / quantity;

    cout << "Из этого бруска можно изготовить " << NumCubes << " кубиков\n";
    cout << "Колличество наборов по "<< quantity <<" кубиков составит " << Num_Set;
}