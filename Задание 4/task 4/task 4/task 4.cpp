﻿#include <iostream>

using namespace std;

int main()
{
	//Задание 4. Убийца Steam
	setlocale(LC_ALL, "Russian");

	int size;
	int speed;
	int file = 0;
	int percent;
	int i = 1;

	cout << "Введите размер файла ";
	cin >> size;
	cout << "Введите скорость загрузки ";
	cin >> speed;

	do
	{
		file = file + speed;
		if (file > size)
			file = size;

		percent = file * 100 / size;

		cout << "Прошло " << i << "сек. Скачано " << file << " из " << size << " (" << percent << "%)\n";
		i++;
	} while (size > file);
}